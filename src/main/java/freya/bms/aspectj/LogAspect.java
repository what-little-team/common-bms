package freya.bms.aspectj;

import freya.bms.annotation.Log;
import freya.bms.common.util.AuthorUtil;
import freya.bms.common.util.IPUtil;
import freya.bms.model.entity.SysApiLog;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * 接口调用日志实现
 *
 * @author jiejiebiezheyang
 * @since 2023-05-20 17:46
 */
@Aspect
@Component
public class LogAspect {

    static Logger logger = LoggerFactory.getLogger(LogAspect.class);

    @Pointcut("@annotation(freya.bms.annotation.Log)")
    void pointcut() {
    }

    @Around("pointcut()")
    private Object testAop(ProceedingJoinPoint point) throws Throwable {
        SysApiLog sysApiLog = new SysApiLog();
        // 获取当前请求
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        // 获取参数
        Object[] args = point.getArgs();
        // 获取目标方法
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        Log log = method.getAnnotation(Log.class);
        Object result = point.proceed();// 执行目标方法 获取返回值
        ObjectMapper mapper = new ObjectMapper();
        // 保存日志
        sysApiLog.setUsername(AuthorUtil.getUserName()); // 调用者
        sysApiLog.setIp(IPUtil.getIP(request)); // 调用ip
        sysApiLog.setUrl(request.getRequestURI()); // 请求路径
        sysApiLog.setTitle(log.title()); // 操作title
        sysApiLog.setMethod(signature.getDeclaringTypeName() + "." + method.getName()); // 调用的方法
        sysApiLog.setRequestType(request.getMethod()); // 请求方式
        sysApiLog.setType(log.businessType().toString()); // 业务类型
        sysApiLog.setParams(mapper.writeValueAsString(args)); // 请求参数
        sysApiLog.setResult(mapper.writeValueAsString(result)); // 返回结果
        sysApiLog.setTime(new Date());
        // 写入日志
        logger.info(sysApiLog.toString());
        return result;
    }
}
