package freya.bms.filter;

import cn.hutool.json.JSONUtil;
import freya.bms.common.ResponseResult;
import freya.bms.common.util.IPUtil;
import freya.bms.common.util.JWTUtils;
import freya.bms.common.util.RedisUtils;
import freya.bms.exception.FreyaException;
import freya.bms.model.entity.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * token认证过滤器
 *
 * @author jiejiebiezheyang
 * @author liuch
 * @since 2023-04-03 14:05
 */
@Component
public class JWTFilter extends OncePerRequestFilter {

    @Autowired
    private RedisUtils redisUtils;

    @Value("${token.expiration}")
    private Integer tokenExpiration;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        // 获取token
        String token = request.getHeader("Authorization");
        if (!StringUtils.hasText(token) || "/user/login".equals(request.getRequestURI())) {
            filterChain.doFilter(request, response);
            return;
        }
        // 解析token,获取userId
        String userId = null;
        try {
            userId = JWTUtils.parseToken(token, "userId", false);
        } catch (Exception e) {
            if (e instanceof FreyaException) {
                response.getWriter().println(JSONUtil.toJsonStr(ResponseResult.err(((FreyaException) e).getCode(), e.getMessage())));
            } else {
                response.getWriter().println(JSONUtil.toJsonStr(ResponseResult.err()));
            }
            return;
        }
        // 从redis中获取用户信息
        LoginUser loginUser = redisUtils.getBean("login:" + userId, LoginUser.class);
        if (loginUser == null || loginUser.getSysUser() == null) {
            response.getWriter().println(JSONUtil.toJsonStr(ResponseResult.err(40012, "无效的会话,或会话已过期")));
            return;
        }
        // 重新设置过期时间
        redisUtils.setBean("login:" + userId, loginUser, tokenExpiration);
        // 将用户信息存入SecurityContext
        IPUtil.getRequest().setAttribute("userId", userId);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        filterChain.doFilter(request, response);
    }
}
