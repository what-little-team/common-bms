package freya.bms.model.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * system mapper
 *
 * @author jiejiebiezheyang
 * @since 2023-06-02 22:55
 */
@Mapper
public interface SystemMapper {
    /**
     * 根据用户获取一级菜单目录
     *
     * @return List<Map < String, Object>>
     */
    List<Map<String, Object>> selectFirstLevelMenu(Integer id);

    /**
     * 根据一级菜单id查询二级菜单
     *
     * @return List<Map < String, Object>>
     */
    List<Map<String, Object>> selectMenuByFirstLevelMenuId(String id);

}
