package freya.bms.model.mapper;

import freya.bms.model.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 针对表【SYS_USER(用户表)】的数据库操作Mapper
 *
 * @author jiejiebiezheyang
 * @since 2023-08-16 11:52
 */
@Mapper
public interface SysUserMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    /**
     * 根据用户名查询
     */
    SysUser selectByUsername(String username);

    /**
     * 根据用户id获取对应权限
     */
    List<String> getAllPurviewByUserId(Integer id);

    /**
     * 更具邮箱查询用户
     */
    SysUser selectByEmail(String email);

    /**
     * 获取除了密码外的用户信息
     */
    SysUser selectByPrimaryKeyExcludePassword(String userId);
}
