package freya.bms.model.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * SYS_API_LOG 实体类
 *
 * @author jiejiebiezheyang
 * @since 2023-05-20 18:46
 */
@Data
public class SysApiLog {
    /**
     * 调用id
     */
    private Integer id;

    /**
     * 调用者
     */
    private String username;

    /**
     * 调用ip
     */
    private String ip;

    /**
     * 请求路径
     */
    private String url;

    /**
     * 操作title
     */
    private String title;

    /**
     * 调用的方法
     */
    private String method;

    /**
     * 请求方式
     */
    private String requestType;

    /**
     * 业务类型
     */
    private String type;

    /**
     * 请求参数
     */
    private String params;

    /**
     * 返回结果
     */
    private String result;

    /**
     * 调用时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date time;
}

