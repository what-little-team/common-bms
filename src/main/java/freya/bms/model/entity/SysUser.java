package freya.bms.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 用户表
 *
 * @author jiejiebiezheyang
 * @since 2023-04-03 22:59
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysUser implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 用户实名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 用户性别
     */
    private String sex;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 用户地址
     */
    private String address;

    /**
     * 用户出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    /**
     * 用户个性签名
     */
    private String userSignature;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 是否注销（0否 1是）
     */
    private String isDel;

    /**
     * 是否禁用（0禁用 1启用）
     */
    private String isDisable;

    private static final long serialVersionUID = 1L;
}