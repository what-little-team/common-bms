package freya.bms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * SpringBoot启动类
 *
 * @author lch
 * @author jiejiebiezheyang
 * @since 2023-04-03 15:25
 */
@EnableOpenApi
@SpringBootApplication
public class FreyaApplication {

    public static void main(String[] args) {
        SpringApplication.run(FreyaApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  平台启动成功   ლ(´ڡ`ლ)ﾞ ");
    }

}
