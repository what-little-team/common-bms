package freya.bms.controller;

import freya.bms.common.ResponseResult;
import freya.bms.service.SystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类的描述
 *
 * @author jiejiebiezheyang
 * @since 2023-06-02 23:21
 */
@Api(tags = "系统相关接口")
@RestController
@RequestMapping("/sys")
public class SystemController {

    @Autowired
    private SystemService systemService;

    @GetMapping("/menu")
    @ApiOperation("获取菜单")
    public ResponseResult menu() {
        return systemService.getSysMenu();
    }

    @GetMapping("/userinfo")
    @ApiOperation("获取当前用户信息")
    public ResponseResult userinfo() {
        return systemService.userinfo();
    }
}
