package freya.bms.controller;

import freya.bms.annotation.Log;
import freya.bms.common.ResponseResult;
import freya.bms.common.enums.BusinessType;
import freya.bms.model.entity.SysUser;
import freya.bms.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 登录相关Controller
 *
 * @author jiejiebiezheyang
 * @since 2023-04-03 22:56
 */
@Api(tags = "登录功能接口")
@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @ApiOperation("登录")
    @PostMapping("/login")
    @Log(title = "用户登录", businessType = BusinessType.OTHER)
    public ResponseResult login(String username, String password) {
        return loginService.login(username, password);
    }

    @ApiOperation("退出登录")
    @PostMapping("/logout")
    @Log(title = "用户退出", businessType = BusinessType.OTHER)
    public ResponseResult logout() {
        return loginService.logout();
    }

    @ApiOperation("用户注册-获取验证码")
    @GetMapping("/register/captcha")
    @Log(title = "用户注册", businessType = BusinessType.OTHER)
    public ResponseResult registerCode(String email) {
        return loginService.registerCode(email);
    }

    @ApiOperation("用户注册")
    @PostMapping("/register")
    @Log(title = "用户注册", businessType = BusinessType.OTHER)
    public ResponseResult register(@RequestBody SysUser sysUser, String uuid, String code) {
        return loginService.register(sysUser, uuid, code);
    }
}
