package freya.bms.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.io.File;

@Component
public class EmailUtil {

    @Value("${spring.mail.from}")
    private String from;

    @Autowired
    private JavaMailSender mailSender;

    /**
     * 发送纯文本邮件信息
     *
     * @param to      接收方
     * @param subject 邮件主题
     * @param content 邮件内容（发送内容）
     */
    public void sendMessage(String to, String subject, String content) {
        // 创建一个邮件对象
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom(from); // 设置发送发
        msg.setTo(to); // 设置接收方
        msg.setSubject(subject); // 设置邮件主题
        msg.setText(content); // 设置邮件内容
        // 发送邮件
        mailSender.send(msg);
    }

    /**
     * 发送HTML邮件信息
     *
     * @param to      接收方
     * @param subject 邮件主题
     * @param content 邮件内容（发送内容）
     */
    public void sendHTMLMessage(String to, String subject, String content) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(from); // 设置发送方
            helper.setTo(to); // 设置接收方
            helper.setSubject(subject); // 设置邮件主题
            helper.setText(content, true); // 设置邮件HTML内容
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 发送邮件
        mailSender.send(mimeMessage);
    }

    /**
     * 发送带附件的邮件信息
     *
     * @param to      接收方
     * @param subject 邮件主题
     * @param content 邮件内容（发送内容）
     * @param files   文件数组 // 可发送多个附件
     */
    public void sendMessageCarryFiles(String to, String subject, String content, File[] files) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(from); // 设置发送发
            helper.setTo(to); // 设置接收方
            helper.setSubject(subject); // 设置邮件主题
            helper.setText(content); // 设置邮件内容
            if (files != null && files.length > 0) { // 添加附件（多个）
                for (File file : files) {
                    helper.addAttachment(file.getName(), file);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 发送邮件
        mailSender.send(mimeMessage);
    }

    /**
     * 发送带附件的邮件信息
     *
     * @param to      接收方
     * @param subject 邮件主题
     * @param content 邮件内容（发送内容）
     * @param file    单个文件
     */
    public void sendMessageCarryFile(String to, String subject, String content, File file) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(from); // 设置发送发
            helper.setTo(to); // 设置接收方
            helper.setSubject(subject); // 设置邮件主题
            helper.setText(content); // 设置邮件内容
            helper.addAttachment(file.getName(), file); // 单个附件
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 发送邮件
        mailSender.send(mimeMessage);
    }

    /**
     * 发送注册验证码
     *
     * @param toEmail 发送给
     * @param code    验证码
     */
    public void sendRegisterCode(String toEmail, String code) {
        String content = "<!DOCTYPE html>\n" +
                "<html lang=\"zh-cmn-Hans\">\n" +
                "  <head>\n" +
                "    <meta charset=\"UTF-8\" />\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n" +
                "    <link\n" +
                "      href=\"https://cdn.bootcdn.net/ajax/libs/normalize/8.0.1/normalize.min.css\"\n" +
                "      rel=\"stylesheet\"\n" +
                "    />\n" +
                "    <title>Freya-Admin</title>\n" +
                "  </head>\n" +
                "  <body>\n" +
                "    <table\n" +
                "      cellpadding=\"0\"\n" +
                "      cellspacing=\"0\"\n" +
                "      style=\"\n" +
                "        max-width: 700px;\n" +
                "        margin: 0 auto;\n" +
                "        border-collapse: collapse;\n" +
                "        border-spacing: 0;\n" +
                "        border-left: 1px solid #e6e8ee;\n" +
                "        border-right: 1px solid #e6e8ee;\n" +
                "        border-bottom: 1px solid #e6e8ee;\n" +
                "      \"\n" +
                "    >\n" +
                "      <tbody>\n" +
                "        <tr style=\"height: 40px; width: 100%; background-color: #20242c\">\n" +
                "          <td style=\"padding-top: 9px; padding-bottom: 5px; padding-left: 20px\">\n" +
                "            <!-- <img\n" +
                "              height=\"22\"\n" +
                "              style=\"height: 22px\"\n" +
                "              src=\"\"\n" +
                "              alt=\"Error\"\n" +
                "            /> -->\n" +
                "          </td>\n" +
                "          <td\n" +
                "            style=\"width: 100%; text-align: right; vertical-align: middle; padding-right: 20px\"\n" +
                "          ></td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <td\n" +
                "            colspan=\"2\"\n" +
                "            style=\"\n" +
                "              font-size: 18px;\n" +
                "              font-weight: bold;\n" +
                "              color: #2d2d2d;\n" +
                "              padding-top: 30px;\n" +
                "              padding-bottom: 30px;\n" +
                "              padding-left: 20px;\n" +
                "              padding-right: 20px;\n" +
                "            \"\n" +
                "          >\n" +
                "            Freya-Admin\n" +
                "          </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <td\n" +
                "            colspan=\"2\"\n" +
                "            style=\"font-size: 12px; color: #2d2d2d; padding-left: 20px; line-height: 17px\"\n" +
                "          >\n" +
                "            尊敬的用户, 您好！\n" +
                "          </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <td\n" +
                "            colspan=\"2\"\n" +
                "            style=\"\n" +
                "              font-size: 12px;\n" +
                "              color: #2d2d2d;\n" +
                "              padding-top: 10px;\n" +
                "              padding-bottom: 20px;\n" +
                "              padding-left: 20px;\n" +
                "              padding-right: 20px;\n" +
                "              word-break: break-all;\n" +
                "              line-height: 17px;\n" +
                "              vertical-align: text-top;\n" +
                "            \"\n" +
                "          >\n" +
                "            欢迎您注册Freya管理平台, 您的验证码为：\n" +
                "          </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <td colspan=\"2\" style=\"padding-bottom: 20px; padding-left: 20px; height: 30px\">\n" +
                "            <table>\n" +
                "              <tbody>\n" +
                "                <tr>\n" +
                "                  <td\n" +
                "                    height=\"30\"\n" +
                "                    width=\"100\"\n" +
                "                    style=\"\n" +
                "                      color: #00f525 !important;\n" +
                "                      font-weight: 600;\n" +
                "                      font-size: 16px;\n" +
                "                      background: linear-gradient(315deg, #4bbfec 0%, #7c3cf3 47%, #c136ec 100%);\n" +
                "                      width: 100px;\n" +
                "                      height: 30px;\n" +
                "                      text-align: center;\n" +
                "                      vertical-align: middle;\n" +
                "                      line-height: 30px;\n" +
                "                    \"\n" +
                "                  >\n" +
                "                    " + code + "\n" +
                "                  </td>\n" +
                "                </tr>\n" +
                "              </tbody>\n" +
                "            </table>\n" +
                "          </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <td\n" +
                "            colspan=\"2\"\n" +
                "            style=\"\n" +
                "              padding-bottom: 40px;\n" +
                "              font-size: 12px;\n" +
                "              color: #2d2d2d;\n" +
                "              padding-left: 20px;\n" +
                "              line-height: 17px;\n" +
                "            \"\n" +
                "          >\n" +
                "            为保证您的隐私请在5分钟内使用\n" +
                "          </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <td\n" +
                "            colspan=\"2\"\n" +
                "            style=\"\n" +
                "              color: #2d2d2d;\n" +
                "              padding-bottom: 15px;\n" +
                "              padding-left: 20px;\n" +
                "              font-size: 12px;\n" +
                "              font-weight: 900;\n" +
                "              line-height: 17px;\n" +
                "            \"\n" +
                "          >\n" +
                "            本邮件由系统自动发送，请勿直接回复。如非本人操作，请忽略此邮件。\n" +
                "          </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <td colspan=\"2\" style=\"padding-left: 20px; padding-right: 20px\">\n" +
                "            <hr style=\"border-top: #eff0f5; margin: 0\" />\n" +
                "          </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <td\n" +
                "            colspan=\"2\"\n" +
                "            height=\"4\"\n" +
                "            style=\"\n" +
                "              width: 100%;\n" +
                "              height: 4px;\n" +
                "              background: linear-gradient(315deg, #4bbfec 0%, #7c3cf3 47%, #c136ec 100%);\n" +
                "            \"\n" +
                "          ></td>\n" +
                "        </tr>\n" +
                "      </tbody>\n" +
                "    </table>\n" +
                "  </body>\n" +
                "</html>\n";

        System.out.println(content);

        sendHTMLMessage(toEmail, "验证码通知", content);
    }
}
