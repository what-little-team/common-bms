package freya.bms.common.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 登录用户信息
 *
 * @author lch mailto:{yulwins@163.com}
 * @since 2023-04-28 10:31
 */
public class AuthorUtil {
    /**
     * 获取当前用户名称
     *
     * @return {@link String}
     */
    public static String getUserName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }
}
