package freya.bms.service;

import freya.bms.common.ResponseResult;

/**
 * @author jiejiebiezheyang
 * @since 2023-06-02 23:29
 */
public interface SystemService {

    ResponseResult getSysMenu();

    ResponseResult userinfo();
}
