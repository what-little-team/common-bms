package freya.bms.service;

import freya.bms.common.ResponseResult;
import freya.bms.model.entity.SysUser;

/**
 * 用户service类
 *
 * @author jiejiebiezheyang
 * @since 2023-04-03 22:58
 */
public interface LoginService {
    ResponseResult login(String username, String password);

    ResponseResult logout();

    ResponseResult registerCode(String email);

    ResponseResult register(SysUser sysUser, String uuid, String code);

}
