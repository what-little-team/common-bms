package freya.bms.service.impl;

import cn.hutool.json.JSONObject;
import freya.bms.common.ResponseResult;
import freya.bms.common.util.IPUtil;
import freya.bms.model.entity.SysUser;
import freya.bms.model.mapper.SysUserMapper;
import freya.bms.model.mapper.SystemMapper;
import freya.bms.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 系统相关
 *
 * @author jiejiebiezheyang
 * @since 2023-06-02 23:30
 */
@Service
public class SystemServiceImpl implements SystemService {

    @Autowired
    private SystemMapper systemMapper;
    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 获取菜单信息
     *
     * @return ResponseResult
     */
    @Override
    public ResponseResult getSysMenu() {
        String userId = String.valueOf(IPUtil.getRequest().getAttribute("userId"));
        // 获取一级菜单
        List<Map<String, Object>> firstLevelMenu = systemMapper.selectFirstLevelMenu(Integer.parseInt(userId));
        // 获取二级菜单
        for (Map<String, Object> map : firstLevelMenu) {
            String id = String.valueOf(map.get("id"));
            List<Map<String, Object>> secondLevelMenu = systemMapper.selectMenuByFirstLevelMenuId(id);
            map.put("secondLevelMenu", secondLevelMenu);
        }
        JSONObject json = new JSONObject();
        json.set("menu", firstLevelMenu);
        return ResponseResult.ok(json);
    }

    /**
     * 获取当前用户信息
     *
     * @return ResponseResult
     */
    @Override
    public ResponseResult userinfo() {
        String userId = String.valueOf(IPUtil.getRequest().getAttribute("userId"));
        SysUser sysUser = sysUserMapper.selectByPrimaryKeyExcludePassword(userId);
        return ResponseResult.ok(sysUser);
    }
}
