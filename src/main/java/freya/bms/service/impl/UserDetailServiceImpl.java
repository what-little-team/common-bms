package freya.bms.service.impl;

import freya.bms.exception.FreyaException;
import freya.bms.model.entity.LoginUser;
import freya.bms.model.entity.SysUser;
import freya.bms.model.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * UserDetailsService实现类, 实现从数据库中获取用户信息
 *
 * @author jiejiebiezheyang
 * @since 2023-04-03 22:30
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public UserDetails loadUserByUsername(String username) {
        // 查询用户信息
        SysUser sysUser = sysUserMapper.selectByUsername(username);
        if (sysUser == null) {
            throw new FreyaException(40016, "不存在此用户");
        }
        if ("1".equals(sysUser.getIsDel())) {
            throw new FreyaException(40016, "此用户已注销");
        }
        if ("1".equals(sysUser.getIsDisable())) {
            throw new FreyaException(40016, "无权限");
        }
        // 查询对应的权限信息
        List<String> purview = sysUserMapper.getAllPurviewByUserId(sysUser.getId());
        // 把数据封装成UserDetail返回
        return new LoginUser(sysUser, purview);
    }
}
