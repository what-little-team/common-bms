# freya-admin

#### 介绍

通用后台管理系统

#### 相关仓库

[common-BMS-UI](https://gitee.com/what-little-team/common-BMS-UI)

#### 软件架构

软件架构说明

#### 安装教程

1. 启动参数: -Djasypt.encryptor.password=Team@2023
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 项目注释规范

```java

/**
 * 文档工具类<br>
 * 可以使用此工具类快速的查看项目文档注释规范
 *
 * @author 作者1
 * @author 作者2
 * @since 2023-04-04 15:25
 */
public class DOCTest {


    /**
     * 方法的描述<br>
     * 这个方法可以将{@link Integer}和{@link String}进行拼接,并最终返回一个{@link String}
     *
     * @param s1 {@link Integer}
     * @param s2 {@link String}
     * @throw {@link Exception}
     * @retrun {@link String}
     */
    public String getString(Integer s1, String s2) throws Exception {
        return s1 + s2;
    }
}

```

#### 错误码列表

| 状态码   | 描述信息         |
|-------|--------------|
| 500   | 服务器内部错误      |
| 40000 | 一般错误         |
| 40010 | token 非法     |
| 40011 | token 过期     |
| 40012 | 无效的会话,或会话已过期 |
| 40013 | 登陆失败         |
| 40014 | 认证异常         |
| 40015 | 权限异常         |
| 40016 | 用户未注册        |
